import time
import csv
import datetime
from random import shuffle
from dataclasses import dataclass
from typing import Dict, List

year = datetime.datetime.now().year - 2000
years = f"{year-1}-{year}"
print(f"years: {years}")

@dataclass
class ProjectInfo:
    ids: List[int]
    prj_username: Dict[int, List[str]]
    prj_student_name: Dict[int, str]
    prj_mail: Dict[int, str]
    mails: List[str]

def read_cvs(filename='etudiants_projets.csv'):
    prjs = []
    prj_user = {}
    prj_user_names = {}
    prj_mails = {}
    etu_prj = []
    r_prj = 3
    r_user = 4
    mails = []
    flag = 0
    with open(filename, newline='') as f:
        reader = csv.reader(f)
        for row in reader:
            print(row)
            etu_prj.append(row)
            if flag:
                if row[r_prj]:
                    prj = int(row[r_prj])
                    if prj not in prjs:
                        prjs.append(prj)
                        prj_user[prj] = []
                        prj_user_names[prj] = ""
                        prj_mails[prj] = ""
                    if row[r_user]:
                        prj_user[prj].append(row[r_user])
                    prj_user_names[prj] += "{} {},".format(row[0], row[1])
                    prj_mails[prj] += "{},".format(row[2])
                    mails.append(row[2])
            else:
                flag = 1
    prjs.sort()
    prj_user_names = {k:v[:-1] for k,v in  prj_user_names.items()}
    prj_mails = {k:v[:-1] for k,v in prj_mails.items()}
    prj_info = ProjectInfo(prjs, prj_user, prj_user_names, prj_mails, mails)
    #return (prjs, prj_user, prj_user_names, prj_mails)
    return prj_info

def wiki_table(prj_info=read_cvs(), years=years):
    subgroups = prj_info.ids
    subgrps_user_names = prj_info.prj_student_name
    table =  ('|class="wikitable alternance"\n'
               '|+ Affectation des projets INFO4 {}\n'
               '|-\n'
               '|\n'
               '!scope="col"| Sujet\n'
               '!scope="col"| Etudiants\n'
               '!scope="col"| Enseignant(s)\n'
               '!scope="col"| Fiche de suivi\n'
               '!scope="col"| Documents\n'
               '|-\n').format(years)

    for subgrp_id in subgroups:
        names = subgrps_user_names[subgrp_id]
        table += (               
            '!scope="row"| {}\n'
            '| [[TODO]]\n'
            '| {}\n'
            '| TODO\n'
            '| [https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/{}/{:02d}/docs/README.md Fiche]\n'
            '| [[Media:xxx.pdf|Rapport final]] - [[Media:xxx.pdf|Presentation finale FR]] - [[Media:xxx.pdf|Final Presentation EN]] - [[Media:xxx.pdf|Flyer]] - [[Media:xxx.pdf|Presentation de mi-parcours]]\n'
            '|-\n').format(subgrp_id, names[:-1], years, int(subgrp_id))

    return '{' + table + '|}'


def passage_groupe(projet_id_max, remove_ids=[], h=14, m=0, duration=10):
    ids = [i for i in range(1, projet_id_max+1)]
    for i in remove_ids:
        ids.remove(i)

    shuffle(ids)
        
    for i, prj_id in enumerate(ids):
        print("{} projet:{}".format(time.strftime('%H:%M', time.gmtime(3600*h + 60*(m+i*duration))),
                                    prj_id))

# python -i scripts/gitlab-info4.py
    
    
