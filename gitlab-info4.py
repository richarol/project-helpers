#!/usr/bin/env python3

# nix-shell -p 'python37.withPackages(ps: with ps; [ python-gitlab ])'
# https://docs.gitlab.com/ee/api/README.html

# https://python-gitlab.readthedocs.io/en/stable/
# 50 => Owner access # Only valid for groups

import gitlab
import datetime
from helpers import read_cvs
from typing import List, Dict

projets_INFO4 =2998
grp_18_19 = 2999
grp_19_20 = 5559
grp_20_21 = 9099
grp_21_22 = 13196

year = datetime.datetime.now().year - 2000
years = f"{year-1}-{year}"
print(f"years: {years}")
prj_info = read_cvs()

def get_token(secret_file='scripts/secret.txt'):
    token = ""
    with open(secret_file) as f:
        token  = f.readline()
    return token[:-1]

gl = gitlab.Gitlab('https://gricad-gitlab.univ-grenoble-alpes.fr/', get_token())
#Projets-INFO4 19-20 Groupe: 5559


def get_grp(grp_id):
    return gl.groups.get(grp_id)

def get_user_id(username):
    users = gl.users.list(search=username)
    if len(users) != 1:
        print('Bad: len(users): {}'.format(len(users)))
        raise ValueError
    return users[0].id

def delete_member(grp, username):
    user_id = get_user_id(username)
    grp.members.delete(user_id)
    
def add_user(grp_name, subgrp_name):
    pass

def get_sub_grp(grp_name, subgrp_name):
    grp = get_grp(grp_name)
    sub_grps = grp.subgroups.list(search=subgrp_name)
    if not sub_grps:
        return []
    elif len(sub_grps) > 1:
        print('Bad: len(sub_grps): {}'.format(len(sub_grps)))
        raise ValueError
    else:
        return sub_grps[0]

def create_grp_prj(grp_name, subgrp_name, usernames):
    #create subproject
    subgrp_name ="{:02d}".format(int(subgrp_name))
    print('Create subproject: {}/{}'.format(grp_name, subgrp_name))
    new_grp = gl.groups.create({'name': subgrp_name, 'path': subgrp_name,
                                'parent_id': str(grp_name),
                                'visibility': 'public'})

    # create docs
    print('Create docs repository')
    project = gl.projects.create({'name': 'docs', 'namespace_id': new_grp.id,
                                  'visibility': 'public'})
    
    # add users
    print('Add users {}'.format(usernames))
    for username in usernames:
        user_id = 0
        try:
            user_id = get_user_id(username)
        except Exception as e:
            print('Failed to retrieve id for: {username}')
        if user_id:
            new_grp.members.create({'user_id': user_id,
                                    'access_level': gitlab.OWNER_ACCESS})


def create_groups_projects(grp_name, subgroups, subgrps_users):    
    for subgrp_id in subgroups:
        if not get_sub_grp(grp_name, str(subgrp_id)):
            create_grp_prj(grp_name, str(subgrp_id), subgrps_users[subgrp_id])
        else:
            print('Skip subgroup creation, {} is already there'.format(subgrp_id))


def create_info4_group(years=years, new_ownernames=[]):
    print(f'Create info4 groupe: ({projets_INFO4}) Projets-INFO4/{years}')
    description = f"Projets étudiants Polytech'Grenoble INFO4 (Saison {years})"
    new_grp = gl.groups.create({'name': years, 'path': years,
                                'parent_id': projets_INFO4,
                                'description': description,
                                'visibility': 'public'})

    print(f'Projets-INFO4/{years} group_id: {new_grp.id}')
    
    for username in new_ownernames:
        new_grp.members.create({'user_id': get_user_id(username),
                                'access_level': gitlab.OWNER_ACCESS})

    return new_grp.id
        
def filter_prj_not_ready(prjs, prjs_not_ready):
    return list(filter(lambda x: x not in prjs_not_ready, prjs))

def get_prj_readness_ids(prjs=prj_info):
    prj_ready_ids = []
    prj_not_ready_ids = []
    for key, value in prjs.prj_username.items():
        if value:
            prj_ready_ids.append(key)
        else:
             prj_not_ready_ids.append(key)
    return (prj_ready_ids, prj_not_ready_ids)

# python -i scripts/gitlab-info4.py

# Create new saisonal Projets_INFO4 
# print(years)
# grp_id = create_info4_group()

# Create all subgroups w/ docs repository
# create_groups_projects(grp_id, prj_info)



### Misc ##############################################

# mail prj
# sb_mails[10]

#grp = get_grp(grp_id)
#grp = get_grp(grp_18_19)
#new_grp = gl.groups.create({'name': '42', 'path': '42', 'parent_id': str(grp_18_19)})
#sub_grp = grp.subgroups.list(search='42')[0]

# Example only one subgroup w/ docs repository
#sb_ids, sb_users, sb_user_names = read_cvs()
#create_groups_projects(grp_19_20, sb_ids[:1], sb_users)
